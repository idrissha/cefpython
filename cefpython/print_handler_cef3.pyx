# PyPrintDialogCallback

cdef PyPrintDialogCallback CreatePyPrintDialogCallback(
        CefRefPtr[CefPrintDialogCallback] cefCallback):
    cdef PyPrintDialogCallback pyPrintDialogCallback = PyPrintDialogCallback()
    pyPrintDialogCallback.cefCallback = cefCallback
    return pyPrintDialogCallback

cdef class PyPrintDialogCallback:
    cdef CefRefPtr[CefPrintDialogCallback] cefCallback
    cdef PyPrintSettings pyPrintSettings
    cpdef py_void Continue(self, object printSettings):
        if isinstance(printSettings, PyPrintSettings):
            self.pyPrintSettings = printSettings
            self.cefCallback.get().Continue(self.pyPrintSettings.GetCefPrintSettings())
        else:
            print "Invalid instance of PyPrintSettings passed"

    cpdef py_void Cancel(self):
        self.cefCallback.get().Cancel()

# PyPrintJobCallback

cdef PyPrintJobCallback CreatePyPrintJobCallback(
        CefRefPtr[CefPrintJobCallback] cefCallback):
    cdef PyPrintJobCallback pyPrintJobCallback = PyPrintJobCallback()
    pyPrintJobCallback.cefCallback = cefCallback
    return pyPrintJobCallback

cdef class PyPrintJobCallback:
    cdef CefRefPtr[CefPrintJobCallback] cefCallback
    cpdef py_void Continue(self):
        self.cefCallback.get().Continue()

cdef public void PrintHandler_OnPrintStart(
        CefRefPtr[CefBrowser] cefBrowser
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef object clientCallback
    # print "IDRIS: Calling OnPrintStart"
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        clientCallback = pyBrowser.GetClientCallback("OnPrintStart")
        if clientCallback:
            clientCallback(pyBrowser)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void PrintHandler_OnPrintSettings(
        CefRefPtr[CefBrowser] cefBrowser,
        CefRefPtr[CefPrintSettings] cefSettings,
        cpp_bool cefGetDefaults
        ) except * with gil:
    # print "IDRIS: Calling OnPrintSettings"
    cdef PyBrowser pyBrowser
    cdef py_bool pyGetDefaults
    cdef object pyPrintSettings
    cdef object clientCallback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyGetDefaults = bool(cefGetDefaults)
        pyPrintSettings = CreatePyPrintSettings(cefSettings)
        clientCallback = pyBrowser.GetClientCallback("OnPrintSettings")
        if clientCallback:
            clientCallback(pyPrintSettings, pyGetDefaults)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public cpp_bool PrintHandler_OnPrintDialog(
        CefRefPtr[CefBrowser] cefBrowser,
        cpp_bool cefHasSelection,
        CefRefPtr[CefPrintDialogCallback] cefCallback
        ) except * with gil:
    # print "IDRIS: Calling OnPrintDialog"
    cdef PyBrowser pyBrowser
    cdef py_bool pyHasSelection
    cdef py_bool returnValue
    cdef PyPrintDialogCallback pyCallback
    cdef object clientCallback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyHasSelection = bool(cefHasSelection)
        pyCallback = CreatePyPrintDialogCallback(cefCallback)
        clientCallback = pyBrowser.GetClientCallback("OnPrintDialog")
        if clientCallback:
            returnValue = clientCallback(pyHasSelection, pyCallback)
            return bool(returnValue)
        else:
            return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public cpp_bool PrintHandler_OnPrintJob(
        CefRefPtr[CefBrowser] cefBrowser,
        const CefString& cefDocumentName,
        const CefString& cefPdfFilePath,
        CefRefPtr[CefPrintJobCallback] cefCallback
        ) except * with gil:
    # print "IDRIS: Calling OnPrintJob"
    cdef PyBrowser pyBrowser
    cdef str pyDocumentName
    cdef str pyPdfFilePath
    cdef PyPrintJobCallback pyCallback
    cdef py_bool returnValue
    cdef object clientCallback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyDocumentName = CefToPyString(cefDocumentName)
        pyPdfFilePath = CefToPyString(cefPdfFilePath)
        pyCallback = CreatePyPrintJobCallback(cefCallback)
        clientCallback = pyBrowser.GetClientCallback("OnPrintJob")
        if clientCallback:
            returnValue = clientCallback(pyDocumentName, pyPdfFilePath, pyCallback)
            return bool(returnValue)
        else:
            return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void PrintHandler_OnPrintReset(
        CefRefPtr[CefBrowser] cefBrowser
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef object clientCallback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        clientCallback = pyBrowser.GetClientCallback("OnPrintReset")
        if clientCallback:
            clientCallback()
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public CefSize PrintHandler_GetPdfPaperSize(
        CefRefPtr[CefBrowser] cefBrowser,
        int cefDeviceUnitsPerInch
        ) except * with gil:
    # print "IDRIS: Calling GetPdfPaperSize"
    cdef PyBrowser pyBrowser
    cdef int pyDeviceUnitsPerInch
    cdef object clientCallback
    cdef list returnValues =[]
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyDeviceUnitsPerInch = int(cefDeviceUnitsPerInch)
        clientCallback = pyBrowser.GetClientCallback("GetPdfPaperSize")
        if clientCallback:
            returnValue = clientCallback(pyDeviceUnitsPerInch)
            if (returnValues and len(returnValues) == 2):
                return CefSize(returnValues[0], returnValues[1])
            else:
                return CefSize()
        else:
            return CefSize()
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

