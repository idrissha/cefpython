# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

# -----------------------------------------------------------------------------
# Globals
# -----------------------------------------------------------------------------

# See StoreCustomCookieVisitor().
cdef object g_customSchemeHandler = weakref.WeakValueDictionary()

# -----------------------------------------------------------------------------
# SchemeHandler
# -----------------------------------------------------------------------------

cdef py_void ValidateCustomSchemeHandler(object customSchemeHandler):
    cdef list methods = ["ProcessRequest", "GetResponseHeaders",
            "ReadResponse", "CanGetCookie", "CanSetCookie", "Cancel"]
    for method in methods:
        if customSchemeHandler and hasattr(customSchemeHandler, method) \
                and callable(getattr(customSchemeHandler, method)):
            # Okay.
            continue
        else:
            raise Exception("customSchemeHandler object is missing method: %s" \
                    % method)

cdef public CefRefPtr[CefResourceHandler] ClientSchemeHandlerFactory_Create() with gil:
    cdef object createSchemeHandler
    cdef object customSchemeHandler
    cdef CefRefPtr[CustomSchemeHandler] schemeHandler
    createSchemeHandler = GetGlobalClientCallback("createSchemeHandler")
    if createSchemeHandler:
        customSchemeHandler = createSchemeHandler()
        if customSchemeHandler:
            ValidateCustomSchemeHandler(customSchemeHandler)
            StoreCustomSchemeHandler(customSchemeHandler)
    schemeHandler = <CefRefPtr[CustomSchemeHandler]?>new CustomSchemeHandler()
    return <CefRefPtr[CefResourceHandler]?>schemeHandler

cdef py_void StoreCustomSchemeHandler(object customSchemeHandler):
   global g_customSchemeHandler
   g_customSchemeHandler = customSchemeHandler

cdef PySchemeHandler GetPySchemeHandler():
    global g_customSchemeHandler
    cdef PySchemeHandler pySchemeHandler
    pySchemeHandler = PySchemeHandler(g_customSchemeHandler)
    return pySchemeHandler


cdef class PySchemeHandler:
    cdef object customSchemeHandler

    def __init__(self, object customSchemeHandler):
        self.customSchemeHandler = customSchemeHandler

    cdef object GetCallback(self, str funcName):
        if self.customSchemeHandler and (
                hasattr(self.customSchemeHandler, funcName) and (
                    callable(getattr(self.customSchemeHandler, funcName)))):
            return getattr(self.customSchemeHandler, funcName)

# ------------------------------------------------------------------------------
# SchemeHandler callbacks
# ------------------------------------------------------------------------------
cdef public cpp_bool CustomSchemeHandler_ProcessRequest(
        CefRefPtr[CefRequest] cefRequest,
        CefRefPtr[CefCallback] cefCallback
        ) except * with gil:
    cdef PySchemeHandler pySchemeHandler
    cdef object customCallback
    cdef py_bool returnValue
    cdef PyRequest pyRequest
    cdef PyCallback pyCallback
    try:
        assert IsThread(TID_IO), "Must be called on the IO thread"
        pySchemeHandler = GetPySchemeHandler()
        pyRequest = CreatePyRequest(cefRequest)
        pyCallback = CreatePyCallback(cefCallback)
        if pySchemeHandler:
            customCallback = pySchemeHandler.GetCallback("ProcessRequest")
            if customCallback:
                returnValue = customCallback(pyRequest, pyCallback)
                return bool(returnValue)
        return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void CustomSchemeHandler_GetResponseHeaders(
        CefRefPtr[CefResponse] cefResponse,
        int64& cefResponseLength,
        CefString& cefRedirectUrl
        ) except * with gil:
    cdef PySchemeHandler pySchemeHandler
    cdef object customCallback
    cdef py_bool returnValue
    cdef PyResponse pyResponse
    cdef list responseLengthOut
    cdef list redirectUrlOut
    try:
        assert IsThread(TID_IO), "Must be called on the IO thread"
        pySchemeHandler = GetPySchemeHandler()
        pyResponse = CreatePyResponse(cefResponse)
        responseLengthOut = [cefResponseLength]
        redirectUrlOut = [CefToPyString(cefRedirectUrl)]
        if pySchemeHandler:
            customCallback = pySchemeHandler.GetCallback("GetResponseHeaders")
            if customCallback:
                returnValue = customCallback(pyResponse, responseLengthOut,
                        redirectUrlOut)
                (&cefResponseLength)[0] = <int64>long(responseLengthOut[0])
                if (redirectUrlOut[0]):
                    PyToCefString(redirectUrlOut[0], cefRedirectUrl)
                return
        return
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public cpp_bool CustomSchemeHandler_ReadResponse(
        void* cefDataOut,
        int bytesToRead,
        int& cefBytesRead,
        CefRefPtr[CefCallback] cefCallback
        ) except * with gil:
    cdef PySchemeHandler pySchemeHandler
    cdef object customCallback
    cdef py_bool returnValue
    cdef list dataOut
    cdef list bytesReadOut
    cdef PyCallback pyCallback
    cdef char* tempData
    cdef int tempDataLength
    cdef int pyBytesRead
    try:
        assert IsThread(TID_IO), "Must be called on the IO thread"
        pySchemeHandler = GetPySchemeHandler()
        dataOut = [""]
        bytesReadOut = [0]
        pyCallback = CreatePyCallback(cefCallback)
        if pySchemeHandler:
            customCallback = pySchemeHandler.GetCallback("ReadResponse")
            if customCallback:
                returnValue = customCallback(dataOut, bytesToRead, bytesReadOut,
                        pyCallback)
                pyBytesRead = int(bytesReadOut[0])
                if dataOut[0] and IsString(dataOut[0]):
                    # The tempData pointer is tied to the lifetime
                    # of dataOut[0] string.
                    tempData = dataOut[0]
                    memcpy(cefDataOut, tempData, len(dataOut[0]))
                    assert pyBytesRead >= 0, "bytesReadOut < 0"
                    (&cefBytesRead)[0] = pyBytesRead
                    # True should be returned now.
                else:
                    (&cefBytesRead)[0] = 0
                    # Either:
                    # 1. True should be returned and callback.Continue()
                    #    called at a later time.
                    # 2. False returned to indicate response completion.
                return bool(returnValue)
        return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)