#include "cefpython_public_api.h"
#include "custom_scheme_handler.h"
bool CustomSchemeHandler::ProcessRequest(CefRefPtr<CefRequest> request,
                              CefRefPtr<CefCallback> callback)
                              OVERRIDE {
    REQUIRE_IO_THREAD();
    return CustomSchemeHandler_ProcessRequest(request, callback);

//    bool handled = false;
//
//    std::string url = request->GetURL();
//    if (strstr(url.c_str(), "handler.html") != NULL) {
//      // Build the response html
//      data_ = "<html><head><title>Client Scheme Handler</title></head>"
//              "<body bgcolor=\"white\">"
//              "This contents of this page page are served by the "
//              "ClientSchemeHandler class handling the client:// protocol."
//              "<br/>You should see an image:"
//              "<br/><img src=\"client://tests/logo.png\"><pre>";
//
//
//      data_.append("</pre><br/>Try the test form:"
//                   "<form method=\"POST\" action=\"handler.html\">"
//                   "<input type=\"text\" name=\"field1\">"
//                   "<input type=\"text\" name=\"field2\">"
//                   "<input type=\"submit\">"
//                   "</form></body></html>");
//
//      handled = true;
//
//      // Set the resulting mime type
//      mime_type_ = "text/html";
//    }
//
//    if (handled) {
//      // Indicate the headers are available.
//      callback->Continue();
//      return true;
//    }
//
//    return false;
}

void CustomSchemeHandler::GetResponseHeaders(CefRefPtr<CefResponse> response,
                                  int64& response_length,
                                  CefString& redirectUrl) OVERRIDE {
    REQUIRE_IO_THREAD();
    CustomSchemeHandler_GetResponseHeaders(response, response_length, redirectUrl);

//    DCHECK(!data_.empty());
//
//    response->SetMimeType(mime_type_);
//    response->SetStatus(200);
//
//    // Set the resulting response length
//    response_length = data_.length();
}

void CustomSchemeHandler::Cancel() OVERRIDE {
    REQUIRE_IO_THREAD();
}

bool CustomSchemeHandler::ReadResponse(void* data_out,
                            int bytes_to_read,
                            int& bytes_read,
                            CefRefPtr<CefCallback> callback)
                            OVERRIDE {
    REQUIRE_IO_THREAD();
    return CustomSchemeHandler_ReadResponse(data_out, bytes_to_read, bytes_read, callback);


//    bool has_data = false;
//    bytes_read = 0;
//
//    if (offset_ < data_.length()) {
//      // Copy the next block of data into the buffer.
//      int transfer_size =
//          std::min(bytes_to_read, static_cast<int>(data_.length() - offset_));
//      memcpy(data_out, data_.c_str() + offset_, transfer_size);
//      offset_ += transfer_size;
//
//      bytes_read = transfer_size;
//      has_data = true;
//    }
//
//    return has_data;
}


// Return a new scheme handler instance to handle the request.
CefRefPtr<CefResourceHandler> ClientSchemeHandlerFactory::Create(CefRefPtr<CefBrowser> browser,
                                               CefRefPtr<CefFrame> frame,
                                               const CefString& scheme_name,
                                               CefRefPtr<CefRequest> request) {
    REQUIRE_IO_THREAD();
    return ClientSchemeHandlerFactory_Create();
    //return new CustomSchemeHandler();
}


void RegisterSchemeHandlers(const CefString& scheme_name, const CefString& domain_name) {
  CefRegisterSchemeHandlerFactory(scheme_name, domain_name,
      new ClientSchemeHandlerFactory());
}
