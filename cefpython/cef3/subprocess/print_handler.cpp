// Copyright (c) 2014 The Chromium Embedded Framework Authors.
// Portions Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
#ifdef BROWSER_PROCESS
#include "cefpython_public_api.h"
#endif
#include "print_handler.h"

#include <vector>
#include "include/base/cef_logging.h"
#include "include/base/cef_macros.h"
#include "include/wrapper/cef_helpers.h"

ClientPrintHandlerGtk::ClientPrintHandlerGtk()
{}
void ClientPrintHandlerGtk::OnPrintStart(CefRefPtr<CefBrowser> browser) {
    browser_ = browser;
#ifdef BROWSER_PROCESS
    PrintHandler_OnPrintStart(browser);
#endif
}

void ClientPrintHandlerGtk::OnPrintSettings(
    CefRefPtr<CefPrintSettings> settings,
    bool get_defaults) {
#ifdef BROWSER_PROCESS
    PrintHandler_OnPrintSettings(browser_, settings, get_defaults);
#endif
}

bool ClientPrintHandlerGtk::OnPrintDialog(
    bool has_selection,
    CefRefPtr<CefPrintDialogCallback> callback) {
#ifdef BROWSER_PROCESS
    return PrintHandler_OnPrintDialog(browser_, has_selection, callback);
#endif
    return true;
}

bool ClientPrintHandlerGtk::OnPrintJob(
    const CefString& document_name,
    const CefString& pdf_file_path,
    CefRefPtr<CefPrintJobCallback> callback) {
#ifdef BROWSER_PROCESS
    return PrintHandler_OnPrintJob(browser_, document_name, pdf_file_path, callback);
#endif
    return true;
}

void ClientPrintHandlerGtk::OnPrintReset() {
#ifdef BROWSER_PROCESS
    PrintHandler_OnPrintReset(browser_);
#endif
}

CefSize ClientPrintHandlerGtk::GetPdfPaperSize(int device_units_per_inch) {

#ifdef BROWSER_PROCESS
    return PrintHandler_GetPdfPaperSize(browser_, device_units_per_inch);
#endif
    return CefSize();
}

