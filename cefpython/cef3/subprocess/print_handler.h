// Copyright (c) 2014 The Chromium Embedded Framework Authors.
// Portions Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CEF_TESTS_CEFCLIENT_BROWSER_PRINT_HANDLER_GTK_H_
#define CEF_TESTS_CEFCLIENT_BROWSER_PRINT_HANDLER_GTK_H_
#pragma once

#include <gtk/gtk.h>
#include <gtk/gtkunixprint.h>

#include "include/cef_print_handler.h"

class ClientPrintHandlerGtk : public CefPrintHandler {
 public:
  ClientPrintHandlerGtk();

  // CefPrintHandler methods.
  void OnPrintStart(CefRefPtr<CefBrowser> browser) OVERRIDE;
  void OnPrintSettings(CefRefPtr<CefPrintSettings> settings,
                       bool get_defaults) OVERRIDE;
  bool OnPrintDialog(
      bool has_selection,
      CefRefPtr<CefPrintDialogCallback> callback) OVERRIDE;
  bool OnPrintJob(const CefString& document_name,
                  const CefString& pdf_file_path,
                  CefRefPtr<CefPrintJobCallback> callback) OVERRIDE;
  void OnPrintReset() OVERRIDE;
  CefSize GetPdfPaperSize(int device_units_per_inch) OVERRIDE;

  IMPLEMENT_REFCOUNTING(ClientPrintHandlerGtk);
  DISALLOW_COPY_AND_ASSIGN(ClientPrintHandlerGtk);
 private:
  CefRefPtr<CefBrowser> browser_;
};

#endif  // CEF_TESTS_CEFCLIENT_BROWSER_PRINT_HANDLER_GTK_H_
