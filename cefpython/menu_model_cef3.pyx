# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

cdef PyMenuModel CreatePyMenuModel(
        CefRefPtr[CefMenuModel] cefMenuModel):
    cdef PyMenuModel pyMenuModel = PyMenuModel()
    pyMenuModel.cefMenuModel = cefMenuModel
    return pyMenuModel

cdef class PyMenuModel:
    cdef CefRefPtr[CefMenuModel] cefMenuModel

    cpdef py_void Clear(self):
        self.cefMenuModel.get().Clear()

    cpdef int GetCount(self) except *:
        return self.cefMenuModel.get().GetCount()

    cpdef py_bool AddSeparator(self):
        return self.cefMenuModel.get().AddSeparator()

    cpdef py_bool AddItem(self, int command_id, str label):
        cdef CefString cefLabel
        PyToCefString(label, cefLabel)
        return self.cefMenuModel.get().AddItem(command_id, cefLabel)

    cpdef py_bool AddCheckItem(self, int command_id, str label):
        cdef CefString cefLabel
        PyToCefString(label, cefLabel)
        return self.cefMenuModel.get().AddCheckItem(command_id, cefLabel)

    cpdef py_bool AddRadioItem(self, int command_id, str label, int group_id):
        cdef CefString cefLabel
        PyToCefString(label, cefLabel)
        return self.cefMenuModel.get().AddRadioItem(command_id, cefLabel, group_id)

    cpdef py_bool InsertSeparatorAt(self, int index):
        return self.cefMenuModel.get().InsertSeparatorAt(index)

    cpdef py_bool InsertItemAt(self, int index, int command_id, str label):
        cdef CefString cefLabel
        PyToCefString(label, cefLabel)
        return self.cefMenuModel.get().InsertItemAt(index, command_id, cefLabel)

    cpdef py_bool InsertCheckItemAt(self, int index, int command_id, str label):
        cdef CefString cefLabel
        PyToCefString(label, cefLabel)
        return self.cefMenuModel.get().InsertCheckItemAt(index, command_id, cefLabel)

    cpdef py_bool InsertRadioItemAt(self, int index, int command_id, str label, int group_id):
        cdef CefString cefLabel
        PyToCefString(label, cefLabel)
        return self.cefMenuModel.get().InsertRadioItemAt(index, command_id, cefLabel, group_id)

    cpdef py_bool Remove(self, int command_id):
        return self.cefMenuModel.get().Remove(command_id)

    cpdef py_bool RemoveAt(self, int index):
        return self.cefMenuModel.get().RemoveAt(index)

    cpdef int GetIndexOf(self, int command_id) except *:
        return self.cefMenuModel.get().GetIndexOf(command_id)

    cpdef int GetCommandIdAt(self, int index) except *:
        return self.cefMenuModel.get().GetCommandIdAt(index)

    cpdef py_bool SetCommandIdAt(self, int index, int command_id):
        return self.cefMenuModel.get().SetCommandIdAt(index, command_id)

    cpdef str GetLabel(self, int command_id):
        return CefToPyString(self.cefMenuModel.get().GetLabel(command_id))

    cpdef str GetLabelAt(self, int index):
        return CefToPyString(self.cefMenuModel.get().GetLabelAt(index))

    cpdef py_bool SetLabel(self, int command_id, str label):
        cdef CefString cefLabel
        PyToCefString(label, cefLabel)
        return self.cefMenuModel.get().SetLabel(command_id, cefLabel)

    cpdef py_bool SetLabelAt(self, int index, str label):
        cdef CefString cefLabel
        PyToCefString(label, cefLabel)
        return self.cefMenuModel.get().SetLabelAt(index, cefLabel)

    cpdef py_bool IsVisible(self, int command_id):
        return self.cefMenuModel.get().IsVisible(command_id)

    cpdef py_bool IsVisibleAt(self, int index):
        return self.cefMenuModel.get().IsVisibleAt(index)

    cpdef py_bool SetVisible(self, int command_id, cpp_bool visible):
        return self.cefMenuModel.get().SetVisible(command_id, visible)

    cpdef py_bool SetVisibleAt(self, int index, cpp_bool visible):
        return self.cefMenuModel.get().SetVisibleAt(index, visible)

    cpdef py_bool IsEnabled(self, int command_id):
        return self.cefMenuModel.get().IsEnabled(command_id)

    cpdef py_bool IsEnabledAt(self, int index):
        return self.cefMenuModel.get().IsEnabledAt(index)

    cpdef py_bool SetEnabled(self, int command_id, cpp_bool enabled):
        return self.cefMenuModel.get().SetEnabled(command_id, enabled)

    cpdef py_bool SetEnabledAt(self, int index, cpp_bool enabled):
        return self.cefMenuModel.get().SetEnabledAt(index, enabled)

    cpdef py_bool IsChecked(self, int command_id):
        return self.cefMenuModel.get().IsChecked(command_id)

    cpdef py_bool IsCheckedAt(self, int index):
        return self.cefMenuModel.get().IsCheckedAt(index)

    cpdef py_bool SetChecked(self, int command_id, cpp_bool checked):
        return self.cefMenuModel.get().SetChecked(command_id, checked)

    cpdef py_bool SetCheckedAt(self, int index, cpp_bool checked):
        return self.cefMenuModel.get().SetCheckedAt(index, checked)

    cpdef py_bool HasAccelerator(self, int command_id):
        return self.cefMenuModel.get().HasAccelerator(command_id)

    cpdef py_bool HasAcceleratorAt(self, int index):
        return self.cefMenuModel.get().HasAcceleratorAt(index)

    cpdef py_bool SetAccelerator(self, int command_id, int key_code, cpp_bool shift_pressed, cpp_bool ctrl_pressed,\
        cpp_bool alt_pressed):
        return self.cefMenuModel.get().SetAccelerator(command_id, key_code, shift_pressed, ctrl_pressed, alt_pressed)

    cpdef py_bool SetAcceleratorAt(self, int index, int key_code, cpp_bool shift_pressed, cpp_bool ctrl_pressed,\
        cpp_bool alt_pressed):
        return self.cefMenuModel.get().SetAcceleratorAt(index, key_code, shift_pressed, ctrl_pressed, alt_pressed)

    cpdef py_bool RemoveAccelerator(self, int command_id):
        return self.cefMenuModel.get().RemoveAccelerator(command_id)

    cpdef py_bool RemoveAcceleratorAt(self, int index):
        return self.cefMenuModel.get().RemoveAcceleratorAt(index)

    cpdef py_bool GetAccelerator(self, int command_id, int& key_code, cpp_bool& shift_pressed, cpp_bool& ctrl_pressed,\
        cpp_bool& alt_pressed):
        return self.cefMenuModel.get().GetAccelerator(command_id, key_code, shift_pressed, ctrl_pressed, alt_pressed)

    cpdef py_bool GetAcceleratorAt(self, int index, int& key_code, cpp_bool& shift_pressed, cpp_bool& ctrl_pressed,\
        cpp_bool& alt_pressed):
        return self.cefMenuModel.get().GetAcceleratorAt(index, key_code, shift_pressed, ctrl_pressed, alt_pressed)

