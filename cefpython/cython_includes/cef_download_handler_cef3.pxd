# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

from cef_base cimport CefBase
from cef_string cimport CefString
#from multimap cimport multimap as cpp_multimap
from libcpp cimport bool as cpp_bool
from cef_types cimport int64, uint32
from cef_time cimport CefTime
#from cef_ptr cimport CefRefPtr

cdef extern from "include/cef_download_handler.h":
    cdef cppclass CefBeforeDownloadCallback:
        void Continue(const CefString& name, cpp_bool display_file_chooser_dialog)

    cdef cppclass CefDownloadItemCallback:
        void Cancel()
        void Pause()
        void Resume()

cdef extern from "include/cef_download_item.h":
    cdef cppclass CefDownloadItem:
        cpp_bool IsValid()
        cpp_bool IsInProgress()
        cpp_bool IsComplete()
        cpp_bool IsCanceled()
        int64 GetCurrentSpeed()
        int GetPercentComplete()
        int64 GetTotalBytes()
        int64 GetReceivedBytes()
        CefTime GetStartTime()
        CefTime GetEndTime()
        CefString GetFullPath()
        uint32 GetId()
        CefString GetURL()
        CefString GetOriginalUrl()
        CefString GetSuggestedFileName()
        CefString GetContentDisposition()
        CefString GetMimeType()