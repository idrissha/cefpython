# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

from cef_base cimport CefBase
from cef_string cimport CefString
from multimap cimport multimap as cpp_multimap
from libcpp cimport bool as cpp_bool
from cef_ptr cimport CefRefPtr

cdef extern from "include/internal/cef_types.h":
    ctypedef enum cef_context_menu_media_type_t:
        CM_MEDIATYPE_NONE = 0,
        CM_MEDIATYPE_IMAGE,
        CM_MEDIATYPE_VIDEO,
        CM_MEDIATYPE_AUDIO,
        CM_MEDIATYPE_FILE,
        CM_MEDIATYPE_PLUGIN,
    ctypedef cef_context_menu_media_type_t MediaType

    ctypedef enum cef_context_menu_type_flags_t:
        CM_TYPEFLAG_NONE        = 0,
        CM_TYPEFLAG_PAGE        = 1 << 0,
        CM_TYPEFLAG_FRAME       = 1 << 1,
        CM_TYPEFLAG_LINK        = 1 << 2,
        CM_TYPEFLAG_MEDIA       = 1 << 3,
        CM_TYPEFLAG_SELECTION   = 1 << 4,
        CM_TYPEFLAG_EDITABLE    = 1 << 5,
    ctypedef cef_context_menu_type_flags_t TypeFlags

    ctypedef enum cef_context_menu_media_state_flags_t:
        CM_MEDIAFLAG_NONE                  = 0,
        CM_MEDIAFLAG_ERROR                 = 1 << 0,
        CM_MEDIAFLAG_PAUSED                = 1 << 1,
        CM_MEDIAFLAG_MUTED                 = 1 << 2,
        CM_MEDIAFLAG_LOOP                  = 1 << 3,
        CM_MEDIAFLAG_CAN_SAVE              = 1 << 4,
        CM_MEDIAFLAG_HAS_AUDIO             = 1 << 5,
        CM_MEDIAFLAG_HAS_VIDEO             = 1 << 6,
        CM_MEDIAFLAG_CONTROL_ROOT_ELEMENT  = 1 << 7,
        CM_MEDIAFLAG_CAN_PRINT             = 1 << 8,
        CM_MEDIAFLAG_CAN_ROTATE            = 1 << 9,
    ctypedef cef_context_menu_media_state_flags_t MediaStateFlags

    ctypedef enum cef_context_menu_edit_state_flags_t:
        CM_EDITFLAG_NONE            = 0,
        CM_EDITFLAG_CAN_UNDO        = 1 << 0,
        CM_EDITFLAG_CAN_REDO        = 1 << 1,
        CM_EDITFLAG_CAN_CUT         = 1 << 2,
        CM_EDITFLAG_CAN_COPY        = 1 << 3,
        CM_EDITFLAG_CAN_PASTE       = 1 << 4,
        CM_EDITFLAG_CAN_DELETE      = 1 << 5,
        CM_EDITFLAG_CAN_SELECT_ALL  = 1 << 6,
        CM_EDITFLAG_CAN_TRANSLATE   = 1 << 7,
    ctypedef cef_context_menu_edit_state_flags_t EditStateFlags

cdef extern from "include/cef_context_menu_handler.h":

    cdef cppclass CefContextMenuParams(CefBase):
        int GetXCoord()
        int GetYCoord()
        TypeFlags GetTypeFlags()
        CefString GetLinkUrl()
        CefString GetUnfilteredLinkUrl()
        CefString GetSourceUrl()
        cpp_bool HasImageContents()
        CefString GetPageUrl()
        CefString GetFrameUrl()
        CefString GetFrameCharset()
        MediaType GetMediaType()
        MediaStateFlags GetMediaStateFlags()
        CefString GetSelectionText()
        cpp_bool IsEditable()
        EditStateFlags GetEditStateFlags()

cdef extern from "include/cef_menu_model.h":
    cdef cppclass CefMenuModel(CefBase):
        void Clear()
        int GetCount()
        cpp_bool AddSeparator()
        cpp_bool AddItem(int command_id, const CefString& label)
        cpp_bool AddCheckItem(int command_id, const CefString& label)
        cpp_bool AddRadioItem(int command_id, const CefString& label, int group_id)
        cpp_bool InsertSeparatorAt(int index)
        cpp_bool InsertItemAt(int index, int command_id, const CefString& label)
        cpp_bool InsertCheckItemAt(int index, int command_id, const CefString& label)
        cpp_bool InsertRadioItemAt(int index, int command_id, const CefString& label, int group_id)
        cpp_bool Remove(int command_id)
        cpp_bool RemoveAt(int index)
        int GetIndexOf(int command_id)
        int GetCommandIdAt(int index)
        cpp_bool SetCommandIdAt(int index, int command_id)
        CefString GetLabel(int command_id)
        CefString GetLabelAt(int index)
        cpp_bool SetLabel(int command_id, const CefString& label)
        cpp_bool SetLabelAt(int index, const CefString& label)
        cpp_bool IsVisible(int command_id)
        cpp_bool IsVisibleAt(int index)
        cpp_bool SetVisible(int command_id, cpp_bool visible)
        cpp_bool SetVisibleAt(int index, cpp_bool Visible)
        cpp_bool IsEnabled(int command_id)
        cpp_bool IsEnabledAt(int index)
        cpp_bool SetEnabled(int command_id, cpp_bool enabled)
        cpp_bool SetEnabledAt(int index, cpp_bool enabled)
        cpp_bool IsChecked(int command_id)
        cpp_bool IsCheckedAt(int index)
        cpp_bool SetChecked(int command_id, cpp_bool checked)
        cpp_bool SetCheckedAt(int index, cpp_bool checked)
        cpp_bool HasAccelerator(int command_id)
        cpp_bool HasAcceleratorAt(int index)
        cpp_bool SetAccelerator(int command_id, int key_code, cpp_bool shift_pressed, cpp_bool ctrl_pressed,\
            cpp_bool alt_pressed)
        cpp_bool SetAcceleratorAt(int index, int key_code, cpp_bool shift_pressed, cpp_bool ctrl_pressed,\
            cpp_bool alt_pressed)
        cpp_bool RemoveAccelerator(int command_id)
        cpp_bool RemoveAcceleratorAt(int index)
        cpp_bool GetAccelerator(int command_id, int& key_code, cpp_bool& shift_pressed, cpp_bool& ctrl_pressed,\
            cpp_bool& alt_pressed)
        cpp_bool GetAcceleratorAt(int index, int& key_code, cpp_bool& shift_pressed, cpp_bool& ctrl_pressed,\
            cpp_bool& alt_pressed)
