# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

from cef_string cimport CefString
from libcpp cimport bool as cpp_bool
from libcpp.vector cimport vector as cpp_vector

cdef extern from "include/cef_dialog_handler.h":
    cdef cppclass CefFileDialogCallback:
        void Continue(cpp_bool success,
                      cpp_vector[CefString]& accepted_filters)
        void Cancel()

