# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

from cef_string cimport CefString
from cef_ptr cimport CefRefPtr
from cef_print_settings_cef3 cimport CefPrintSettings

cdef extern from "include/cef_print_handler.h":
    cdef cppclass CefPrintDialogCallback:
        void Continue(CefRefPtr[CefPrintSettings] settings)
        void Cancel()
    cdef cppclass CefPrintJobCallback:
        void Continue()
