include "compile_time_constants.pxi"

cdef extern from "include/cef_version.h":

    IF CEF_VERSION == 3:
        cdef int cef_version_info(int entry) nogil
