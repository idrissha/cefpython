include "compile_time_constants.pxi"

from cef_resource_handler_cef3 cimport CefResourceHandler
from cef_string cimport CefString

cdef extern from "client_handler/custom_scheme_handler.h":
    cdef cppclass CustomSchemeHandler(CefResourceHandler):
        pass

    cdef void RegisterSchemeHandlers(const CefString& scheme_name, const CefString& domain_name)