# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

from cef_base cimport CefBase
from cef_ptr cimport CefRefPtr
from cef_string cimport CefString
from cef_types_wrappers cimport CefPageRange, CefSize, CefRect
from cef_types cimport cef_urlrequest_flags_t, cef_postdataelement_type_t, cef_color_model_t, cef_duplex_mode_t
from libcpp.vector cimport vector as cpp_vector
from libcpp cimport bool as cpp_bool
from multimap cimport multimap as cpp_multimap

ctypedef cef_color_model_t ColorModel
ctypedef cef_duplex_mode_t DuplexMode
cdef extern from "include/cef_print_settings.h":
    # This types won't be visible in pyx files!
    # ctypedef cpp_multimap[CefString, CefString] HeaderMap
    # ctypedef cef_urlrequest_flags_t CefRequestFlags
    ctypedef cpp_vector[CefPageRange] PageRangeList

    cdef CefRefPtr[CefPrintSettings] CefPrintSettings_Create "CefPrintSettings::Create"()
    cdef cppclass CefPrintSettings(CefBase):
        cpp_bool IsValid()
        cpp_bool IsReadOnly()
        CefRefPtr[CefPrintSettings] Copy()
        void SetOrientation(cpp_bool landscape)
        cpp_bool IsLandscape()
        void SetPrinterPrintableArea(const CefSize& physical_size_device_units,
                const CefRect& printable_area_device_units,
                cpp_bool landscape_needs_flip)
        void SetDeviceName(const CefString& name)
        CefString GetDeviceName()
        void SetDPI(int dpi)
        int GetDPI()
        void SetPageRanges(const PageRangeList& ranges)
        size_t GetPageRangesCount()
        void GetPageRanges(PageRangeList& ranges)
        void SetSelectionOnly(cpp_bool selection_only)
        cpp_bool IsSelectionOnly()
        void SetCollate(cpp_bool collate)
        cpp_bool WillCollate()
        void SetColorModel(ColorModel model)
        ColorModel GetColorModel()
        void SetCopies(int copies)
        int GetCopies()
        void SetDuplexMode(DuplexMode mode)
        DuplexMode GetDuplexMode()

