# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

# -----------------------------------------------------------------------------
# PyFileDialogCallback
# -----------------------------------------------------------------------------
cdef PyFileDialogCallback CreatePyFileDialogCallback(
        CefRefPtr[CefFileDialogCallback] cefCallback):
    cdef PyFileDialogCallback pyCallback = PyFileDialogCallback()
    pyCallback.cefCallback = cefCallback
    return pyCallback

cdef class PyFileDialogCallback:
    cdef CefRefPtr[CefFileDialogCallback] cefCallback

    cpdef py_void Continue(self, int selected_accept_filters, list accept_filters):
        assert(type(accept_filters) == list)
        cdef cpp_vector[CefString] cefAcceptFilters
        cdef CefString cefString
        cdef cpp_vector[CefString].iterator iterator
        iterator = cefAcceptFilters.begin()
        for i, pyString in enumerate(accept_filters):
            PyToCefString(pyString, cefString)
            iterator = cefAcceptFilters.insert(iterator, cefString)
        self.cefCallback.get().Continue(selected_accept_filters,
                                        cefAcceptFilters)

    cpdef py_void Cancel(self):
        self.cefCallback.get().Cancel()
# -----------------------------------------------------------------------------
# FileDialogHandler
# -----------------------------------------------------------------------------

cdef public cpp_bool FileDialogHandler_OnFileDialog(
        CefRefPtr[CefBrowser] cefBrowser,
        cef_types.cef_file_dialog_mode_t dialog_mode,
        const CefString& title,
        const CefString& default_file_path,
        cpp_vector[CefString] accept_filters,
        int selected_accept_filter,
        CefRefPtr[CefFileDialogCallback] callback
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_string pyTitle
    cdef py_string pyDefaultFilePath
    cdef list pyAcceptFilters = []
    cdef CefString cefString
    cdef cpp_vector[CefString].iterator iterator
    cdef PyFileDialogCallback pyCallback

    cdef object clientCallback
    cdef py_bool returnValue
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyTitle = CefToPyString(title)
        pyDefaultFilePath = CefToPyString(default_file_path)
        pyCallback = CreatePyFileDialogCallback(callback)
        clientCallback = pyBrowser.GetClientCallback("OnFileDialog")
        if clientCallback:
            iterator = accept_filters.begin()
            while iterator != accept_filters.end():
                cefString = deref(iterator)
                pyAcceptFilters.append(CefToPyString(cefString))
                preinc(iterator)
            returnValue = clientCallback(pyBrowser, dialog_mode, pyTitle,
                    pyDefaultFilePath, pyAcceptFilters, selected_accept_filter, pyCallback)
            return bool(returnValue)
        return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)
