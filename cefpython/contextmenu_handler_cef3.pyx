# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

# cef_termination_status_t
# TS_ABNORMAL_TERMINATION = cef_types.TS_ABNORMAL_TERMINATION
# TS_PROCESS_WAS_KILLED = cef_types.TS_PROCESS_WAS_KILLED
# TS_PROCESS_CRASHED = cef_types.TS_PROCESS_CRASHED

# -----------------------------------------------------------------------------
# PyAuthCallback
# -----------------------------------------------------------------------------

cdef public void ContextMenuHandler_OnBeforeContextMenu(
        CefRefPtr[CefBrowser] cefBrowser,
        CefRefPtr[CefFrame] cefFrame,
        CefRefPtr[CefContextMenuParams] cefContextMenuParams,
        CefRefPtr[CefMenuModel] cefMenuModel
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef PyFrame pyFrame
    cdef PyMenuModel pyMenuModel
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyFrame = GetPyFrame(cefFrame)
        pyMenuModel = CreatePyMenuModel(cefMenuModel)
        pyContextMenuParams = CreateContextMenuParams(cefContextMenuParams)
        clientCallback = pyBrowser.GetClientCallback("OnBeforeContextMenu")
        if clientCallback:
            clientCallback(pyBrowser, pyFrame, pyContextMenuParams, pyMenuModel)
        # print "IDRIS: ContextMenuHandler_OnBeforeContextMenu called"
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public cpp_bool ContextMenuHandler_OnContextMenuCommand(
        CefRefPtr[CefBrowser] cefBrowser,
        CefRefPtr[CefFrame] cefFrame,
        CefRefPtr[CefContextMenuParams] cefContextMenuParams,
        int command_id,
        int event_flags
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef PyFrame pyFrame
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyFrame = GetPyFrame(cefFrame)
        pyContextMenuParams = CreateContextMenuParams(cefContextMenuParams)
        clientCallback = pyBrowser.GetClientCallback("OnContextMenuCommand")
        if clientCallback:
            returnValue = clientCallback(pyBrowser, pyFrame, pyContextMenuParams, command_id, event_flags)
            return bool(returnValue)
        else:
            return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void ContextMenuHandler_OnContextMenuDismissed(
        CefRefPtr[CefBrowser] cefBrowser,
        CefRefPtr[CefFrame] cefFrame
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef PyFrame pyFrame
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyFrame = GetPyFrame(cefFrame)
        clientCallback = pyBrowser.GetClientCallback("OnContextMenuDismissed")
        if clientCallback:
            clientCallback(pyBrowser, pyFrame)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef PyContextMenuParams CreateContextMenuParams( \
        CefRefPtr[CefContextMenuParams]cefContextMenuParams):
    cdef PyContextMenuParams pyContextMenuParams = PyContextMenuParams()
    pyContextMenuParams.cefContextMenuParams = cefContextMenuParams
    return pyContextMenuParams

cdef class PyContextMenuParams:
    cdef CefRefPtr[CefContextMenuParams] cefContextMenuParams

    cpdef int GetXCoord(self) except *:
        return self.cefContextMenuParams.get().GetXCoord()

    cpdef int GetYCoord(self) except *:
        return self.cefContextMenuParams.get().GetXCoord()

    cpdef int GetTypeFlags(self) except *:
        return self.cefContextMenuParams.get().GetTypeFlags()

    cpdef str GetLinkUrl(self):
        return CefToPyString(self.cefContextMenuParams.get().GetLinkUrl())

    cpdef str GetUnfilteredLinkUrl(self):
        return CefToPyString(self.cefContextMenuParams.get().GetUnfilteredLinkUrl())

    cpdef str GetSourceUrl(self):
        return CefToPyString(self.cefContextMenuParams.get().GetSourceUrl())

    cpdef py_bool HasImageContents(self):
        return self.cefContextMenuParams.get().HasImageContents()

    cpdef str GetPageUrl(self):
        return CefToPyString(self.cefContextMenuParams.get().GetPageUrl())

    cpdef str GetFrameUrl(self):
        return CefToPyString(self.cefContextMenuParams.get().GetFrameUrl())

    cpdef str GetFrameCharset(self):
        return CefToPyString(self.cefContextMenuParams.get().GetFrameCharset())

    cpdef int GetMediaType(self) except *:
        return self.cefContextMenuParams.get().GetMediaType()

    cpdef int GetMediaStateFlags(self) except *:
        return self.cefContextMenuParams.get().GetMediaStateFlags()

    cpdef str GetSelectionText(self):
        return CefToPyString(self.cefContextMenuParams.get().GetSelectionText())

    cpdef py_bool IsEditable(self):
        return self.cefContextMenuParams.get().IsEditable()

    cpdef int GetEditStateFlags(self) except *:
        return self.cefContextMenuParams.get().GetEditStateFlags()