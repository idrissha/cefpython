# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

cdef int browser_count = 0
cdef public cpp_bool LifespanHandler_OnBeforePopup(
        CefRefPtr[CefBrowser] cefBrowser,
        CefRefPtr[CefFrame] cefFrame,
        const CefString& targetUrl,
        const CefString& targetFrameName,
        const CefPopupFeatures& cefPopupFeatures,
        CefWindowInfo& windowInfo,
        CefRefPtr[CefClient]& client,
        CefBrowserSettings& settings,
        cpp_bool* noJavascriptAccess
        ) except * with gil:
    # Empty place-holders: popupFeatures, client.
    cdef PyBrowser pyBrowser
    cdef PyFrame pyFrame,
    cdef py_string pyTargetUrl
    cdef py_string pyTargetFrameName
    cdef list pyNoJavascriptAccess # out bool pyNoJavascriptAccess[0]
    cdef list pyWindowInfo
    cdef list pyBrowserSettings
    cdef dict pyPopupFeatures
    cdef object callback
    cdef py_bool returnValue
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyFrame = GetPyFrame(cefFrame)
        pyTargetUrl = CefToPyString(targetUrl)
        pyTargetFrameName = CefToPyString(targetFrameName)
        pyNoJavascriptAccess = [noJavascriptAccess[0]]
        pyWindowInfo = []
        pyBrowserSettings = []
        pyPopupFeatures = {}
        callback = pyBrowser.GetClientCallback("OnBeforePopup")
        if callback:
            pyPopupFeatures["x"] = cefPopupFeatures.x
            pyPopupFeatures["xSet"] = cefPopupFeatures.xSet
            pyPopupFeatures["y"] = cefPopupFeatures.y
            pyPopupFeatures["ySet"] = cefPopupFeatures.ySet
            pyPopupFeatures["width"] = cefPopupFeatures.width
            pyPopupFeatures["widthSet"] = cefPopupFeatures.widthSet
            pyPopupFeatures["height"] = cefPopupFeatures.height
            pyPopupFeatures["heightSet"] = cefPopupFeatures.heightSet
            pyPopupFeatures["menuBarVisible"] = cefPopupFeatures.menuBarVisible
            pyPopupFeatures["statusBarVisible"] = cefPopupFeatures.statusBarVisible
            pyPopupFeatures["toolBarVisible"] = cefPopupFeatures.toolBarVisible
            pyPopupFeatures["locationBarVisible"] = cefPopupFeatures.locationBarVisible
            pyPopupFeatures["scrollbarsVisible"] = cefPopupFeatures.scrollbarsVisible
            pyPopupFeatures["resizable"] = cefPopupFeatures.resizable
            pyPopupFeatures["fullscreen"] = cefPopupFeatures.fullscreen
            pyPopupFeatures["dialog"] = cefPopupFeatures.dialog
            returnValue = bool(callback(pyBrowser, pyFrame, pyTargetUrl,
                    pyTargetFrameName, pyPopupFeatures, pyWindowInfo, None,
                    pyBrowserSettings, pyNoJavascriptAccess))
            noJavascriptAccess[0] = <cpp_bool>bool(pyNoJavascriptAccess[0])
            if len(pyBrowserSettings):
                SetBrowserSettings(pyBrowserSettings[0], &settings)
            if len(pyWindowInfo):
                SetCefWindowInfo(windowInfo, pyWindowInfo[0])
            return bool(returnValue)
        return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void LifespanHandler_OnAfterCreated(
        CefRefPtr[CefBrowser] cefBrowser
        ) except * with gil:
    global browser_count
    cdef PyBrowser pyBrowser
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        callback = GetGlobalClientCallback("OnAfterCreated")
        if callback:
            callback(pyBrowser)
        browser_count += 1
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public cpp_bool LifespanHandler_RunModal(
        CefRefPtr[CefBrowser] cefBrowser
        ) except * with gil:
    cdef PyBrowser pyBrowser
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        callback = pyBrowser.GetClientCallback("RunModal")
        if callback:
            return bool(callback(pyBrowser))
        return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public cpp_bool LifespanHandler_DoClose(
        CefRefPtr[CefBrowser] cefBrowser
        ) except * with gil:
    cdef PyBrowser pyBrowser
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        callback = pyBrowser.GetClientCallback("DoClose")
        if callback:
            return bool(callback(pyBrowser))
        return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void LifespanHandler_OnBeforeClose(
        CefRefPtr[CefBrowser] cefBrowser
        ) except * with gil:
    global browser_count
    cdef PyBrowser pyBrowser
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        callback = pyBrowser.GetClientCallback("OnBeforeClose")
        if callback:
            callback(pyBrowser)
        browser_count -= 1
        if browser_count == 0:
            CefQuitMessageLoop();
        RemovePythonCallbacksForBrowser(pyBrowser.GetIdentifier())
        RemovePyFramesForBrowser(pyBrowser.GetIdentifier())
        RemovePyBrowser(pyBrowser.GetIdentifier())
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)
