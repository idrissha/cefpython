# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

# cef_termination_status_t
# TS_ABNORMAL_TERMINATION = cef_types.TS_ABNORMAL_TERMINATION
# TS_PROCESS_WAS_KILLED = cef_types.TS_PROCESS_WAS_KILLED
# TS_PROCESS_CRASHED = cef_types.TS_PROCESS_CRASHED

# -----------------------------------------------------------------------------
# PyAuthCallback
# -----------------------------------------------------------------------------

cdef public void DownloadHandler_OnBeforeDownload(
        CefRefPtr[CefBrowser] cefBrowser,
        CefRefPtr[CefDownloadItem] cefDownloadItem,
        const CefString& cefSuggestedName,
        CefRefPtr[CefBeforeDownloadCallback] cefBeforeDownloadCallback
        ) except * with gil:
    #cdef PyBrowser pyBrowser
    #cdef PyFrame pyFrame
    cdef PyMenuModel pyMenuModel
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyDownloadItem = CreatePyDownloadItem(cefDownloadItem)
        pySuggestedName = CefToPyString(cefSuggestedName)
        pyBeforeDownloadCallback = CreatePyBeforeDownloadCallback(cefBeforeDownloadCallback)
        clientCallback = pyBrowser.GetClientCallback("OnBeforeDownload")
        if clientCallback:
            clientCallback(pyBrowser, pyDownloadItem, pySuggestedName, pyBeforeDownloadCallback)
    #    print "IDRIS: ContextMenuHandler_OnBeforeContextMenu called"
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public cpp_bool DownloadHandler_OnDownloadUpdated(
        CefRefPtr[CefBrowser] cefBrowser,
        CefRefPtr[CefDownloadItem] cefDownloadItem,
        CefRefPtr[CefDownloadItemCallback] cefCallback
        ) except * with gil:
    cdef PyBrowser pyBrowser
    # cdef PyFrame pyFrame
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyDownloadItem = CreatePyDownloadItem(cefDownloadItem)
        pyDownloadItemCallback = CreatePyDownloadItemCallback(cefCallback)
        # pyFrame = GetPyFrame(cefFrame)
        # pyContextMenuParams = CreateContextMenuParams(cefContextMenuParams)
        clientCallback = pyBrowser.GetClientCallback("OnDownloadUpdated")
        if clientCallback:
            clientCallback(pyBrowser, pyDownloadItem, pyDownloadItemCallback)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef PyDownloadItem CreatePyDownloadItem(CefRefPtr[CefDownloadItem] cefDownloadItem):
    cdef PyDownloadItem pyDownloadItem = PyDownloadItem()
    pyDownloadItem.cefDownloadItem = cefDownloadItem
    return pyDownloadItem

cdef class PyDownloadItem:
    cdef CefRefPtr[CefDownloadItem] cefDownloadItem

    cpdef py_bool IsValid(self):
        return self.cefDownloadItem.get().IsValid()
    cpdef py_bool IsInProgress(self):
        return self.cefDownloadItem.get().IsInProgress()
    cpdef py_bool IsComplete(self):
        return self.cefDownloadItem.get().IsComplete()
    cpdef py_bool IsCanceled(self):
        return self.cefDownloadItem.get().IsCanceled()
    cpdef int64 GetCurrentSpeed(self):
        return self.cefDownloadItem.get().GetCurrentSpeed()
    cpdef int GetPercentComplete(self) except *:
        return self.cefDownloadItem.get().GetPercentComplete()
    cpdef int64 GetTotalBytes(self):
        return self.cefDownloadItem.get().GetTotalBytes()
    cpdef int64 GetReceivedBytes(self):
        return self.cefDownloadItem.get().GetReceivedBytes()
    # cpdef object GetStartTime(self):
    #     return CefTimeTToDatetime(self.cefDownloadItem.get().GetStartTime())
    # cpdef object GetEndTime(self):
    #     return CefTimeTToDatetime(self.cefDownloadItem.get().GetEndTime())
    cpdef str GetFullPath(self):
        return CefToPyString(self.cefDownloadItem.get().GetFullPath())
    cpdef uint32 GetId(self):
        return self.cefDownloadItem.get().GetId()
    cpdef str GetURL(self):
        return CefToPyString(self.cefDownloadItem.get().GetURL())
    cpdef str GetOriginalUrl(self):
        return CefToPyString(self.cefDownloadItem.get().GetOriginalUrl())
    cpdef str GetSuggestedFileName(self):
        return CefToPyString(self.cefDownloadItem.get().GetSuggestedFileName())
    cpdef str GetContentDisposition(self):
        return CefToPyString(self.cefDownloadItem.get().GetContentDisposition())
    cpdef str GetMimeType(self):
        return CefToPyString(self.cefDownloadItem.get().GetMimeType())



cdef PyBeforeDownloadCallback CreatePyBeforeDownloadCallback(
    CefRefPtr[CefBeforeDownloadCallback] cefCallback):
    cdef PyBeforeDownloadCallback pyBeforeDownloadCallback = PyBeforeDownloadCallback()
    pyBeforeDownloadCallback.cefCallback = cefCallback
    return pyBeforeDownloadCallback

cdef class PyBeforeDownloadCallback:
    cdef CefRefPtr[CefBeforeDownloadCallback] cefCallback

    cpdef py_void Continue(self, str name, py_bool display_file_chooser_dialog):
        cdef CefString cefName
        PyToCefString(name, cefName)
        self.cefCallback.get().Continue(cefName, display_file_chooser_dialog)

cdef PyDownloadItemCallback CreatePyDownloadItemCallback(
    CefRefPtr[CefDownloadItemCallback] cefCallback):
    cdef PyDownloadItemCallback pyDownloadItemCallback = PyDownloadItemCallback()
    pyDownloadItemCallback.cefCallback = cefCallback
    return pyDownloadItemCallback

cdef class PyDownloadItemCallback:
    cdef CefRefPtr[CefDownloadItemCallback] cefCallback

    cpdef py_void Cancel(self):
        self.cefCallback.get().Cancel()
    cpdef py_void Pause(self):
        self.cefCallback.get().Pause()
    cpdef py_void Resume(self):
        self.cefCallback.get().Resume()
