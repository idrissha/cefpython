cdef PyPrintSettings CreatePyPrintSettings(
        CefRefPtr[CefPrintSettings] cefPrintSettings):
    cdef PyPrintSettings pyPrintSettings = PyPrintSettings()
    pyPrintSettings.cefPrintSettings = cefPrintSettings
    return pyPrintSettings

cdef class PyPrintSettings:
    cdef CefRefPtr[CefPrintSettings] cefPrintSettings

    cdef CefRefPtr[CefPrintSettings] GetCefPrintSettings(self) except *:
        if<void*> self.cefPrintSettings != NULL and self.cefPrintSettings.get():
            return self.cefPrintSettings
        raise Exception("PyPrintSettings.GetCefPrintSettings() failed: ")

    cpdef py_bool IsValid(self):
        return self.GetCefPrintSettings().get().IsValid()

    cpdef py_bool IsReadOnly(self):
        return self.GetCefPrintSettings().get().IsReadOnly()

    cpdef object Copy(self):
        return CreatePyPrintSettings(self.GetCefPrintSettings().get().Copy())

    cpdef py_void SetOrientation(self, cpp_bool landscape):
        self.GetCefPrintSettings().get().SetOrientation(landscape)

    cpdef py_bool IsLandscape(self):
        return self.GetCefPrintSettings().get().IsLandscape()

    cpdef py_void SetPrinterPrintableArea(self, list device_size,
            list printable_area, cpp_bool landscape_needs_flip):
        cdef CefSize cefSize
        cdef CefRect cefRect
        try:
            if (device_size and len(device_size) == 2):
                cefSize = CefSize(int(device_size[0]), int(device_size[1]))
            else:
                print "Device size not valid"
                return
            if (printable_area and len(printable_area) == 4):
                cefRect = CefRect(int(printable_area[0]), int(printable_area[1]),
                                  int(printable_area[2]), int(printable_area[3]))
            else:
                print "Printable area is not valid"
                return
            self.GetCefPrintSettings().get().SetPrinterPrintableArea(
                    cefSize, cefRect, landscape_needs_flip)
        except:
            (exc_type, exc_value, exc_trace) = sys.exc_info()
            sys.excepthook(exc_type, exc_value, exc_trace)

    cpdef py_void SetDeviceName(self, str name):
        cdef CefString cefName
        PyToCefString(name, cefName)
        self.GetCefPrintSettings().get().SetDeviceName(cefName)

    cpdef str GetDeviceName(self):
        return CefToPyString(self.GetCefPrintSettings().get().GetDeviceName())

    cpdef py_void SetDPI(self, int dpi):
        self.GetCefPrintSettings().get().SetDPI(dpi)

    cpdef int GetDPI(self) except *:
        return self.GetCefPrintSettings().get().GetDPI()

    cpdef py_void SetSelectionOnly(self, cpp_bool selection_only):
        self.GetCefPrintSettings().get().SetSelectionOnly(selection_only)

    cpdef py_bool IsSelectionOnly(self):
        return self.GetCefPrintSettings().get().IsSelectionOnly()

    cpdef py_void SetCollate(self, cpp_bool collate):
        self.GetCefPrintSettings().get().SetCollate(collate)

    cpdef py_bool WillCollate(self):
        return self.GetCefPrintSettings().get().WillCollate()

    cpdef py_void SetCopies(self, int copies):
        self.GetCefPrintSettings().get().SetCopies(copies)

    cpdef int GetCopies(self) except *:
        return self.GetCefPrintSettings().get().GetCopies()

    cpdef py_void SetColorModel(self, int model):
        self.GetCefPrintSettings().get().SetColorModel(<ColorModel>model)

    cpdef int GetColorModel(self) except *:
        return int(self.GetCefPrintSettings().get().GetColorModel())

    cpdef py_void SetDuplexMode (self, int mode):
        self.GetCefPrintSettings().get().SetDuplexMode((<DuplexMode>mode))

    cpdef int GetDuplexMode(self)except *:
        return int(self.GetCefPrintSettings().get().GetDuplexMode())