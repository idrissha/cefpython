# Copyright (c) 2012-2014 The CEF Python authors. All rights reserved.
# License: New BSD License.
# Website: http://code.google.com/p/cefpython/

cdef public void DisplayHandler_OnAddressChange(
        CefRefPtr[CefBrowser] cefBrowser,
        CefRefPtr[CefFrame] cefFrame,
        const CefString& cefUrl
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef PyFrame pyFrame
    cdef py_string pyUrl
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyFrame = GetPyFrame(cefFrame)
        pyUrl = CefToPyString(cefUrl)
        callback = pyBrowser.GetClientCallback("OnAddressChange")
        if callback:
            callback(pyBrowser, pyFrame, pyUrl)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void DisplayHandler_OnTitleChange(
        CefRefPtr[CefBrowser] cefBrowser,
        const CefString& cefTitle
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_string pyTitle
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyTitle = CefToPyString(cefTitle)
        callback = pyBrowser.GetClientCallback("OnTitleChange")
        if callback:
            callback(pyBrowser, pyTitle)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public cpp_bool DisplayHandler_OnTooltip(
        CefRefPtr[CefBrowser] cefBrowser,
        CefString& cefText
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_string pyText
    cdef list pyTextOut
    cdef object callback
    cdef py_bool returnValue
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyText = CefToPyString(cefText)
        pyTextOut = [pyText]
        callback = pyBrowser.GetClientCallback("OnTooltip")
        if callback:
            returnValue = callback(pyBrowser, pyTextOut)
            # pyText and pyTextOut[0] are not the same strings!
            PyToCefString(pyTextOut[0], cefText)
            return bool(returnValue)
        return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void DisplayHandler_OnStatusMessage(
        CefRefPtr[CefBrowser] cefBrowser,
        const CefString& cefValue
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_string pyValue
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyValue = CefToPyString(cefValue)
        callback = pyBrowser.GetClientCallback("OnStatusMessage")
        if callback:
            callback(pyBrowser, pyValue)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public cpp_bool DisplayHandler_OnConsoleMessage(
        CefRefPtr[CefBrowser] cefBrowser,
        const CefString& cefMessage,
        const CefString& cefSource,
        int line
        ) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_string pyMessage
    cdef py_string pySource
    cdef py_bool returnValue
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyMessage = CefToPyString(cefMessage)
        pySource = CefToPyString(cefSource)
        callback = pyBrowser.GetClientCallback("OnConsoleMessage")
        if callback:
            returnValue = callback(pyBrowser, pyMessage, pySource, line)
            return bool(returnValue)
        return False
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void DisplayHandler_OnFaviconURLChange(
        CefRefPtr[CefBrowser] cefBrowser,
        cpp_vector[CefString] cefIconUrls) except * with gil:
    cdef PyBrowser pyBrowser
    cdef list pyIconUrls = []
    cdef cpp_vector[CefString].iterator iterator
    cdef CefString cefString
    cdef object callback
    # print "IDRIS: DisplayHandler_OnFaviconURLChange"
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        callback = pyBrowser.GetClientCallback("OnFaviconURLChange")
        if callback:
            iterator = cefIconUrls.begin()
            while iterator != cefIconUrls.end():
                cefString = deref(iterator)
                pyIconUrls.append(CefToPyString(cefString))
                preinc(iterator)
            callback(pyBrowser, pyIconUrls)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void DisplayHandler_OnFullscreenModeChange(
        CefRefPtr[CefBrowser] cefBrowser,
        cpp_bool cefIsFullscreen) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_bool pyIsFullscreen
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyIsFullscreen = bool(cefIsFullscreen)
        callback = pyBrowser.GetClientCallback("OnFullscreenModeChange")
        if callback:
            callback(pyBrowser, pyIsFullscreen)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void DisplayHandler_OnAudioStatusChange(
        CefRefPtr[CefBrowser] cefBrowser,
        cpp_bool cefIsAudioPlaying) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_bool pyIsAudioPlaying
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyIsAudioPlaying = bool(cefIsAudioPlaying)
        callback = pyBrowser.GetClientCallback("OnAudioStatusChange")
        if callback:
            callback(pyBrowser, pyIsAudioPlaying)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void DisplayHandler_OnVideoRectUpdate(
        CefRefPtr[CefBrowser] cefBrowser,
        cpp_bool cefHasVideo,
        const CefRect& cefRect) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_bool pyHasVideo
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyHasVideo = bool(cefHasVideo)
        callback = pyBrowser.GetClientCallback("OnVideoRectUpdate")
        if callback:
            pyRect = [cefRect.x, cefRect.y, cefRect.width, cefRect.height]
            callback(pyBrowser, pyHasVideo, pyRect)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void DisplayHandler_OnHoverEditableRect(
        CefRefPtr[CefBrowser] cefBrowser,
        cpp_bool cefIsHoverOverEditableRect) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_bool pyIsHoverOverEditableRect
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyIsHoverOverEditableRect = bool(cefIsHoverOverEditableRect)
        callback = pyBrowser.GetClientCallback("OnHoverEditableRect")
        if callback:
            callback(pyBrowser, pyIsHoverOverEditableRect)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)

cdef public void DisplayHandler_OnSelectText(
        CefRefPtr[CefBrowser] cefBrowser,
        const CefString& cefText) except * with gil:
    cdef PyBrowser pyBrowser
    cdef py_string pyText
    cdef object callback
    try:
        pyBrowser = GetPyBrowser(cefBrowser)
        pyText = CefToPyString(cefText)
        callback = pyBrowser.GetClientCallback("OnSelectText")
        if callback:
            callback(pyBrowser, pyText)
    except:
        (exc_type, exc_value, exc_trace) = sys.exc_info()
        sys.excepthook(exc_type, exc_value, exc_trace)